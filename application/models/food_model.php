<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Food_model extends CI_Model {

    public function getFood(){
        $this->load->database();
		$sql = "SELECT * FROM food";
        $query = $this->db->query($sql);
        return $query;
    }
}